// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';

import 'models/Expense.dart';

class SingleExpenseItem extends StatelessWidget {
  final Expense expense;
  final Function deleteExpense;

  const SingleExpenseItem({
    Key? key,
    required this.expense,
    required this.deleteExpense,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isLandScape =
        MediaQuery.of(context).orientation == Orientation.landscape;

    return ListTile(
      leading: Container(
        width: 60,
        height: 60,
        child: CircleAvatar(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: FittedBox(
                child: Text("\$ ${expense.amount.toStringAsFixed(2)}")),
          ),
        ),
      ),
      title: Text(expense.title),
      subtitle: Text(DateFormat.yMMMd().format(expense.date)),
      trailing: TextButton.icon(
          label: Text(
            isLandScape ? "Delete" : "",
            style: TextStyle(color: Theme.of(context).colorScheme.error),
          ),
          onPressed: () {
            deleteExpense(expense);
          },
          icon: Icon(
            Icons.delete,
            color: Theme.of(context).errorColor,
          )),
    );
  }
}
