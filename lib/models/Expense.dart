// ignore_for_file: public_member_api_docs, sort_constructors_first
class Expense {
  final String id;
  final double amount;
  final String title;
  final DateTime date;
  Expense({
    required this.id,
    required this.amount,
    required this.title,
    required this.date,
  });
}
