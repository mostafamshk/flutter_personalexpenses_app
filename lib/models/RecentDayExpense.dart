// ignore_for_file: public_member_api_docs, sort_constructors_first
class RecentDayExpense {
  String day;
  double dayTotalAmount;

  RecentDayExpense({
    required this.day,
    required this.dayTotalAmount,
  });
}
