// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'package:personalexpenses/SingleDayColumn.dart';
import 'package:personalexpenses/models/Expense.dart';

import 'models/RecentDayExpense.dart';

class WeeklyChart extends StatelessWidget {
  final List<Expense> expenses;
  final double totalExpense;

  const WeeklyChart({
    Key? key,
    required this.expenses,
    required this.totalExpense,
  }) : super(key: key);

  List<RecentDayExpense> getRecentlyExpenses() {
    List<RecentDayExpense> result = [];
    for (int i = 0; i < 7; i++) {
      DateTime today = DateTime.now().subtract(Duration(days: i));
      List<Expense> singleDayExpenses = expenses.where((e) {
        DateTime expenseDate = e.date;
        return expenseDate.year == today.year &&
            expenseDate.month == today.month &&
            expenseDate.day == today.day;
      }).toList();

      double sum = singleDayExpenses.fold(
          0, (previousValue, element) => previousValue + element.amount);

      result.add(RecentDayExpense(
          day: ["M", "T", "W", "Th", "F", "S", "Su"][today.weekday - 1],
          dayTotalAmount: sum));
    }

    return result.reversed.toList();
  }

  @override
  Widget build(BuildContext context) {
    final isLandScape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    List<RecentDayExpense> recentlyExpenses = getRecentlyExpenses();

    return Card(
      elevation: 5,
      child: Container(
        width: double.infinity,
        height: isLandScape
            ? MediaQuery.of(context).size.height - 130
            : MediaQuery.of(context).size.height * 1 / 5,
        padding: EdgeInsets.all(8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: recentlyExpenses
              .map((e) => SignleDayColumn(
                    recentDayExpense: e,
                    totalExpense: totalExpense,
                  ))
              .toList(),
        ),
      ),
    );
  }
}
