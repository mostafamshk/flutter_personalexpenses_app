// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';

import 'package:personalexpenses/SingleExpenseItem.dart';

import 'models/Expense.dart';

class ExpencesList extends StatelessWidget {
  final List<Expense> expenses;
  final Function deleteExpense;

  const ExpencesList({
    Key? key,
    required this.expenses,
    required this.deleteExpense,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isLandScape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    return Container(
      height: MediaQuery.of(context).size.height - (isLandScape ? 40 : 290),
      decoration: BoxDecoration(),
      child: ListView.builder(
        itemBuilder: (context, index) {
          return SingleExpenseItem(
            deleteExpense: deleteExpense,
            expense: expenses.elementAt(index),
          );
        },
        itemCount: expenses.length,
      ),
    );
  }
}
