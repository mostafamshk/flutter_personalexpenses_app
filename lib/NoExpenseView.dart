import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class NoExpenseView extends StatelessWidget {
  const NoExpenseView({super.key});

  @override
  Widget build(BuildContext context) {
    final isLandScape =
        MediaQuery.of(context).orientation == Orientation.landscape;

    return Container(
      padding: EdgeInsets.only(bottom: 100, top: 50),
      width: double.infinity,
      child: ListView(children: [
        Container(
          height:
              MediaQuery.of(context).size.height - (isLandScape ? 250 : 300),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "No Expense Record Available!",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              !isLandScape
                  ? Icon(
                      Icons.monetization_on,
                      size: 200,
                      color: Theme.of(context).colorScheme.primary,
                    )
                  : Container(),
              SizedBox(
                height: 30,
              ),
              Text(
                "Add One",
                style: TextStyle(fontSize: 20),
              ),
              Icon(
                Icons.arrow_downward,
                size: 40,
              )
            ],
          ),
        ),
      ]),
    );
  }
}
