import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:personalexpenses/AddExpenseForm.dart';
import 'package:personalexpenses/ExpensesList.dart';
import 'package:personalexpenses/NoExpenseView.dart';
import 'package:personalexpenses/WeeklyChart.dart';
import 'package:personalexpenses/models/Expense.dart';
import './models/RecentDayExpense.dart';
import 'package:intl/intl.dart';

class PersonalExpenses extends StatefulWidget {
  const PersonalExpenses({super.key});

  @override
  State<PersonalExpenses> createState() => _PersonalExpensesState();
}

class _PersonalExpensesState extends State<PersonalExpenses>
    with WidgetsBindingObserver {
  final List<Expense> expenses = [];
  double totalExpense = 0;
  List<RecentDayExpense> recentlyExpenses = [];
  bool showChart = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("AAAAAAAAAAAAAAAAAAAAAAAAAA ${state}");
    super.didChangeAppLifecycleState(state);
  }

  void openAddingForm() {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return AddExpenseForm(
          addExpense: addExpense,
        );
      },
    );
  }

  void deleteExpense(Expense expense) {
    setState(() {
      totalExpense -= expense.amount;
      expenses.removeWhere((element) => element.id == expense.id);
    });
  }

  void addExpense(String title, String amount, DateTime? date) {
    if (title == "" || amount == "" || date == null) {
      return;
    }
    Expense newExpense = Expense(
        amount: double.parse(amount),
        title: title,
        date: date,
        id: DateTime.now().toString());

    setState(() {
      totalExpense += double.parse(amount);
      expenses.add(newExpense);
      Navigator.of(context).pop();
    });
  }

  @override
  Widget build(BuildContext context) {
    final isLandScape =
        MediaQuery.of(context).orientation == Orientation.landscape;

    return Scaffold(
      appBar: AppBar(title: Text("Personal Expenses"), actions: [
        isLandScape && expenses.isNotEmpty
            ? Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Show Weekly Chart"),
                  Switch(
                    activeColor: Theme.of(context).colorScheme.secondary,
                    value: showChart,
                    onChanged: (value) {
                      setState(() {
                        showChart = value;
                      });
                    },
                  ),
                ],
              )
            : Container(),
        IconButton(
            onPressed: () {
              openAddingForm();
            },
            icon: Icon(Icons.add))
      ]),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          openAddingForm();
        },
        child: Icon(Icons.add),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: expenses.isEmpty
            ? NoExpenseView()
            : ListView(
                scrollDirection: Axis.vertical,
                children: [
                  !isLandScape || (isLandScape && showChart)
                      ? WeeklyChart(
                          expenses: expenses,
                          totalExpense: totalExpense,
                        )
                      : SizedBox.shrink(),
                  SizedBox(
                    height: 10,
                  ),
                  !isLandScape || (isLandScape && !showChart)
                      ? ExpencesList(
                          deleteExpense: deleteExpense,
                          expenses: expenses,
                        )
                      : SizedBox.shrink()
                ],
              ),
      ),
    );
  }
}
