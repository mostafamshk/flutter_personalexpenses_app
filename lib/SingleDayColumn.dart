// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'package:personalexpenses/models/RecentDayExpense.dart';

class SignleDayColumn extends StatelessWidget {
  final RecentDayExpense recentDayExpense;
  final double totalExpense;

  const SignleDayColumn({
    Key? key,
    required this.recentDayExpense,
    required this.totalExpense,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (ctx, constraints) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            FittedBox(
              fit: BoxFit.cover,
              child: Text(
                "\$ ${recentDayExpense.dayTotalAmount.toStringAsFixed(2)}",
                style: TextStyle(fontSize: 10),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              width: 15,
              height: constraints.maxHeight * (5 / 8),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Theme.of(context).colorScheme.primary, width: 1),
                  shape: BoxShape.rectangle,
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10)),
              child: FractionallySizedBox(
                  alignment: Alignment.bottomCenter,
                  heightFactor: recentDayExpense.dayTotalAmount / totalExpense,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.primary,
                        borderRadius: BorderRadius.circular(10)),
                  )),
            ),
            SizedBox(
              height: 5,
            ),
            Text(recentDayExpense.day)
          ],
        );
      },
    );
  }
}
