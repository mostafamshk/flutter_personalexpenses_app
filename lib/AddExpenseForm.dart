// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AddExpenseForm extends StatefulWidget {
  final Function addExpense;

  const AddExpenseForm({
    Key? key,
    required this.addExpense,
  }) : super(key: key);

  @override
  State<AddExpenseForm> createState() => _AddExpenseFormState();
}

class _AddExpenseFormState extends State<AddExpenseForm> {
  final titleInputController = TextEditingController();
  final amoutInputController = TextEditingController();
  DateTime? _selectedDate;

  void onPickDateButtonTap() {
    showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(2010),
            lastDate: DateTime.now())
        .then((value) {
      setState(() {
        _selectedDate = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400,
      padding: EdgeInsets.only(
          left: 40,
          right: 40,
          top: 20,
          bottom: MediaQuery.of(context).viewInsets.bottom + 20),
      decoration: BoxDecoration(),
      child: ListView(
        scrollDirection: Axis.vertical,
        children: [
          Text(
            "Add Your Expense",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          SizedBox(
            height: 20,
          ),
          TextField(
            controller: titleInputController,
            decoration: InputDecoration(
              label: Text("Title"),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          TextField(
            keyboardType: TextInputType.number,
            controller: amoutInputController,
            decoration: InputDecoration(label: Text("Amount(\$)")),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Date : ${_selectedDate == null ? "No Date Chosen!" : DateFormat.yMMMd().format(_selectedDate!)}",
                style: TextStyle(
                    color: _selectedDate == null
                        ? Colors.grey
                        : Theme.of(context).colorScheme.primary,
                    fontSize: 12),
              ),
              TextButton(
                  onPressed: () {
                    onPickDateButtonTap();
                  },
                  child: Text(
                    "Choose Date!",
                    style: TextStyle(fontSize: 12),
                  ))
            ],
          ),
          SizedBox(
            height: 20,
          ),
          ElevatedButton(
              onPressed: () {
                widget.addExpense(titleInputController.text,
                    amoutInputController.text, _selectedDate);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 18.0),
                child: Text("Add Expense!"),
              ))
        ],
      ),
    );
  }
}
